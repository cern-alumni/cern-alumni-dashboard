### About
This repo is the project for the openshift.

### Build the project
```shell
# Create panel image
$ cd panel
$ docker build -t gitlab-registry.cern.ch/cern-alumni/cern-alumni-dashboard/panel .
$ docker push gitlab-registry.cern.ch/cern-alumni/cern-alumni-dashboard/panel
$ cd ..
# Create nginx image
$ cd nginx
$ docker build -t gitlab-registry.cern.ch/cern-alumni/cern-alumni-dashboard/nginx .
$ docker push gitlab-registry.cern.ch/cern-alumni/cern-alumni-dashboard/nginx
$ cd ..
# Create deepstream image
$ cd deepstream
$ docker build -t gitlab-registry.cern.ch/cern-alumni/cern-alumni-dashboard/deepstream .
$ docker push gitlab-registry.cern.ch/cern-alumni/cern-alumni-dashboard/deepstream
$ cd ..
# Build it
$ kompose up
```
