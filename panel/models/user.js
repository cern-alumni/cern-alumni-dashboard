var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

var UserSchema = new Schema({
  EmailAddress: { type: String, required: true, index: { unique: true } },
  CommonName: { type: String },
  role: { type: String},
  DisplayName: { type: String },
  name: { type: String},
  // PhoneNumber: { type: String},
  // Building: { type: String},
  Firstname: { type: String },
  Lastname: { type: String },
  // Department: { type: String},
  // HomeInstitute: { type: String},
  // PersonID: { type: String, required: true, index: { unique: true } },
  // uidNumber: { type: String, required: true, index: { unique: true } },
  // gidNumber: { type: String, required: true, index: { unique: true } },
  // PreferredLanguage: { type: String},
  // Federation: { type: String},
  // AuthLevel: { type: String},
  // accessToken: { type: String, required: true, index: { unique: true } },
  // refreshToken: { type: String, required: true, index: { unique: true } }
});



UserSchema.pre('save', function(next) {
  var user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('accessToken')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err) return next(err);

      // hash the password using our new salt
      bcrypt.hash(user.accessToken, salt, function(err, hash) {
          if (err) return next(err);

          // override the cleartext password with the hashed one
          user.accessToken = hash;
          next();
      });
  });
});

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.accessToken, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);
