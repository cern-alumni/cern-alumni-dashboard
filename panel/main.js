const express = require('express')
const path = require('path')
const axios = require('axios');
const webpack = require('webpack')
const logger = require('./build/lib/logger')
const project = require('./project.config')
const compress = require('compression')
var session = require('express-session');
const bodyParser = require('body-parser')
// var mongoose = require("mongoose")
var _ = require("underscore")
var async = require('async')
var passport = require('passport')
  , OAuth2Strategy = require('passport-oauth').OAuth2Strategy;


var check_egroups = process.env.CHECK_EGROUPS || false
// var allowed_egroups = process.env.ALLOWED_EGROUPS || [ "networking-mobile-app", "cern-alumni-app-ops", "nationality-GR-cern", "alumni-members-all"]
// var allowed_egroups = process.env.ALLOWED_EGROUPS || ''
var allowed_egroups = process.env.ALLOWED_EGROUPS || 'alumni-activated-accounts'

allowed_egroups =  allowed_egroups.split(',')

passport.use('provider', new OAuth2Strategy({
    authorizationURL: 'https://oauth.web.cern.ch/OAuth/Authorize',
    tokenURL: 'https://oauth.web.cern.ch/OAuth/Token',
    clientID: process.env.OAUTH_CLIENTID || 'CHANGE',
    clientSecret: process.env.OAUTH_CLIENTSECRET || 'CHANGE',
    callbackURL: process.env.OAUTH_CALLBACK_URL || 'CHANGE'
  },
  function(accessToken, refreshToken, profile, done) {
    axios.get("https://oauthresource.web.cern.ch/api/Me", {
      headers: {
        'Authorization': 'Bearer '+accessToken
      }
    }).then(res => {
      if (res.data) {
        resolveCERNRepsonse(res.data, function(err, data) {
          data['accessToken'] = accessToken;
          data['refreshToken'] = refreshToken;

          var userId = data['EmailAddress']
          var user_egroups = [];

          if (data && data['Group'].indexOf(_.union(['alumni-members-all'], allowed_egroups)) > -1 ) {
            user_egroups = [data['Group']]
          }

          axios
            .get("https://oauthresource.web.cern.ch/api/Groups", { headers: { 'Authorization': 'Bearer '+accessToken } })
            .then(
              (res) => {
                if (res && res.data && res.data.groups) {
                  user_egroups = _.union(user_egroups, res.data.groups);
                }

                var intersected_groups = _.intersection(user_egroups, allowed_egroups);
                if (intersected_groups.length == 0) {
                  done(null, false);
                  return;
                }

                done(null, data);
              },
              (err) => {
                done(null, false)
                return;
              }
            )
          return;
        });
      }
      else {
        done(null, false)
        return;
      }
    })
  }
));


var resolveCERNRepsonse = function (data, callback) {
  var result = {};
  async.each(
    data,
    function(i,cb){
      var key = i['Type'].split('/')
      key = key[key.length-1]
      result[key] = i['Value']
      cb()
    },
    function(err){
      if (err) callback(err, null);

      callback(null, result)
    })
}

passport.serializeUser(function(user, done) {
  // placeholder for custom user serialization
  // null is for errors
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  // placeholder for custom user deserialization.
  // maybe you are going to get the user from mongo by id?
  // null is for errors
  done(null, user);
});

const app = express()
app.use(compress())
app.use(bodyParser.json())

app.use(session({secret: "-- ENTER CUSTOM SESSION SECRET --"}));
app.use(passport.initialize());
app.use(passport.session());

app.get('/ping', function (req, res) {
  res.send('DS DASHOARD PONG')
})

app.get('/login', passport.authenticate('provider', { scope: ['Name','Email'] }));

app.post('/login/ds', function(req, res){
  console.log(req.body.authData)
  var authData = req.body.authData;


  if (req.body.authData.type && req.body.authData.type == "webhook") {

    // check if user_id and hash/token match with DB
    User.findOne({ uidNumber: authData.userId }).then(function(user, err) {
      if(err) {
        console.log(err);  // handle errors!
      }
      if (!err && user !== null) {
        // user.comparePassword(authData.token, function(error, isMatch){
        //   if (err) {
        //     res.json({
        //       clientData: {
        //         user_type: "guest"
        //       },
        //       serverData: { role: 'guest' }
        //     })
        //   }

        //   console.log("USER FOUND:  ", isMatch, error)

          if ( authData.token == user.accessToken) {
            if ( ["pamfilos.fokianos@cern.ch"].indexOf(user.EmailAddress) > -1 ){
              res.json({
                userId: user.uidNumber,
                clientData: {
                  user_type: "user",
                  EmailAddress: user.EmailAddress,
                  DisplayName: user.DisplayName,
                  name: user.name,
                  uidNumber: user.uidNumber
                },
                serverData: { role: 'admin' }
              })
            }
            else {
              res.json({
                userId: user.uidNumber,
                clientData: {
                  user_type: "user",
                  EmailAddress: user.EmailAddress,
                  DisplayName: user.DisplayName,
                  name: user.name,
                  uidNumber: user.uidNumber
                },
                serverData: { role: 'user' }
              })
            }
          }
          else {
            res.json({
              clientData: {
                user_type: "guest"
              },
              serverData: { role: 'guest' }
            })
          }
        // })
        // done(null, user);
      }
      else {
        res.json({
          clientData: {
            user_type: "guest"
          },
          serverData: { role: 'guest' }
        })
      }
    });
  }
  else {
    res.json({
      clientData: {
        user_type: "guest"
      },
      serverData: { role: 'guest' }
    })
  }
});
// The OAuth 2.0 provider has redirected the user back to the application.
// Finish the authentication process by attempting to obtain an access
// token.  If authorization was granted, the user will be logged in.
// Otherwise, authentication has failed.
app.get('/api/oauth/authorized/cern/',
  passport.authenticate('provider', { failureRedirect: 'CERNalumni://login?error=1' }),
  function(req, res) {
    return res.redirect('CERNalumni://login?user=' + JSON.stringify(req.user));
  });

// This rewrites all routes requests to the root /index.html file
// (ignoring file requests). If you want to implement universal
// rendering, you'll want to remove this middleware.
app.use('*', function (req, res, next) {
  res.end(403)
  // const filename = path.join(compiler.outputPath, 'index.html')
  // compiler.outputFileSystem.readFile(filename, (err, result) => {
  //   if (err) {
  //     return next(err)
  //   }
  //   res.set('content-type', 'text/html')
  //   res.send(result)
  //   res.end()
  // })
})

module.exports = app
